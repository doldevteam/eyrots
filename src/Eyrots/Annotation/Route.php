<?php
namespace Eyrots\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Route
{
  public $url;
  public $action;
  public $methods;

  public function __construct(array $values, $target = null)
  {
    $this->url = $values['url'] ?? null;
    if(null == $this->url)
    {
      throw new Excpetion("Missing required annotation URL for Route");
    }
    
    $this->methods = is_array(($methods = $values['methods'] ?? ['GET'])) ? $methods : explode(',', $values['methods']);

    if(isset($values['action']))
    {
      $this->action = $values['action'];
    }
  }

  public function setAction($action)
  {
    if($action instanceof \ReflectionMethod)
    {
      $this->action = sprintf("%s:%s", $action->class, $action->name);
    }
  }
}