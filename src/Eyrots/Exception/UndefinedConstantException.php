<?php
namespace Eyrots\Exception;

class UndefinedConstantException extends \Exception
{
  public function __construct($constantName, $code = 0, \Throwable $previous)
  {
    $message = sprintf("Undefied %s constant that is needed by Eyrots.", $constantName);
    parent::__construct($message, $code, $previous);
  }
}