<?php
namespace Eyrots;

use \Slim\App as SlimApp;

class App extends SlimApp
{
  protected $routingProvider;

  public function __construct($container)
  {
    if(is_array($container))
    {
      $container = new Container($container);
    }

    $container->register(new Provider\BaseAppConfigProvider);
    $container->register(new Provider\LoggerProvider);
    $container->register(($conf = new Provider\ConfigProvider));
    
    foreach($container['settings']['providers'] ?? [] as $p)
    {
      $className = '\\' . \str_replace('.', '\\', $p);
      if(method_exists($className, 'configurationFilters'))
      {
        call_user_func(sprintf("%s::configurationFilters", $className), $container);
      }
    }
    
    $conf->parseConfig();

    $this->routingProvider = new Provider\RoutesProvider;
    
    $container->register($this->routingProvider);

    foreach($container['settings']['providers'] ?? [] as $p)
    {
      $className = '\\' . \str_replace('.', '\\', $p);
      $container->register(new $className);
    }

    parent::__construct($container);

    $this->routingProvider->mapRoutes($this);

    $this->options('/{routes:.+}', function ($request, $response, $args)
      {
        return $response;
      }
    );

    foreach($container['settings']['middlewares'] ?? [] as $m)
    {
      $className = '\\' . \str_replace('.', '\\', $m);
      $this->add(new $className);
    }
  }
}