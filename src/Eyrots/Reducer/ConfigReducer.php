<?php
namespace Eyrots\Reducer;

use \Symfony\Component\Yaml\Yaml;

class ConfigReducer
{
  protected $filters = [];
  protected $postFilters = [];
  protected static $configFiles = [];
  protected static $instance = null;

  protected function __construct()
  {

  }
  public static function getInstance()
  {
    if(null == static::$instance)
    {
      static::$instance = new static;
    }
    return static::$instance;
  }

  public static function addFilter($entry, $filter, $priority = 10)
  {
    $instance = static::getInstance();
    $filters = $instance->filters[$entry] ?? [];
    $filters[$priority] = $filter;
    $instance->filters[$entry] = $filters;
  }

  public static function addPostFilter($filter, $priority = 10)
  {
    $instance = static::getInstance();
    while(isset($instance->$postFilters[$priority]))
    {
      $priority++;
    }
    $instance->postFilters[$priority] = $filter;
  }

  public static function getFilters()
  {
    $instance = static::getInstance();
    return $instance->filters;
  }

  public static function getPostFilters()
  {
    $instance = static::getInstance();
    return $instance->postFilters;
  }

  public static function parse($file, $applyFilters = true)
  {
    $fileId = md5($file);
    $config = static::configReducer(
      static::$configFiles[$fileId] ?? (static::$configFiles[$fileId] = Yaml::parseFile($file))
    );
    
    if($applyFilters)
    {
      $filters = static::getFilters();

      foreach($config as $key => $value)
      {
        if(isset($filters[$key]))
        {
          ksort($filters[$key]);
          foreach($filters[$key] as $filter)
          {
            $value = $filter($value);
          }
          $config[$key] = $value;
        }
      }

      $postFilters = static::getPostFilters();
      ksort($postFilters);
      foreach($postFilters as $filter)
      {
        $config = $filter($config);
      }
    }
    return $config;
  }

  public static function configReducer($value, $key = '')
  {
    if(is_array($value) && array_keys($value) !== range(0, count($value)-1, 1))
    {
      $returner = [];
      foreach($value as $k => $v)
      {
        $tmp = static::configReducer($v, sprintf($key == '' ? "%s%s" : "%s.%s", $key, $k));
        if(array_keys($tmp) === range(0, count($tmp)-1, 1))
        {
          $returner[$tmp[0]] = $tmp[1];
        }
        else
        {
          $returner = array_merge($returner, $tmp);
        }
      }
      return $returner;
    }
    else
    {
      return [$key, $value];
    }
  }
}