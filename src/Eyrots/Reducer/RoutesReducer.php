<?php
namespace Eyrots\Reducer;

use \Symfony\Component\Yaml\Yaml;

class RoutesReducer
{
  public static function parse($file)
  {
    $config = Yaml::parseFile($file);

    $routes = [];

    foreach(static::routeReducer($config) as $k => $v)
    {
      $routes[substr($k, 0, 1) == '/' ? $k : sprintf("/%s", $k)] = $v;
    }

    return $routes;
  }

  public static function routeReducer($value, $key = '')
  {
    if(is_array($value) && array_keys($value) !== range(0, count($value)-1, 1))
    {
      $returner = [];
      foreach($value as $k => $v)
      {
        if($k == 'action' || $k == 'methods')
        {
          $returner[$key][$k] = $v;
          continue;
        }
        $tmp = static::routeReducer($v, sprintf($key == '' ? "%s%s" : "%s/%s", $key, $k));
        if(array_keys($tmp) === range(0, count($tmp)-1, 1))
        {
          $returner[$tmp[0]] = $tmp[1];
        }
        else
        {
          $returner = array_merge($returner, $tmp);
        }
      }
      return $returner;
    }
    else if(!empty($value))
    {
      return [$key, $value];
    }
    else
    {
      return null;
    }
  }
}