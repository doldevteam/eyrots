<?php
namespace Eyrots;

use \Slim\Container as SlimContainer;

class Container extends SlimContainer
{
  /**
   * Default settings
   *
   * @var array
   */
  private $defaultSettings = [
      'httpVersion' => '1.1',
      'responseChunkSize' => 4096,
      'outputBuffering' => 'append',
      'determineRouteBeforeAppMiddleware' => false,
      'displayErrorDetails' => false,
      'addContentLengthHeader' => true,
      'routerCacheFile' => false,
  ];

  public function __construct(array $values = [])
  {
    parent::__construct($values);
    $container = $this;
    Reducer\ConfigReducer::addFilter('dirs.controllers', function($value) use($container){
      if(substr($value, 0, 1) != '/')
      {
        return $container['settings']['appbase'] . '/' . $value;
      }
      return $value;
    });
    Reducer\ConfigReducer::addFilter('dirs.cache', function($value) use($container){
      if(substr($value, 0, 1) != '/')
      {
        return $container['settings']['appbase'] . '/' . $value;
      }
      return $value;
    });
  }
}