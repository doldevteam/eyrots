<?php
namespace Eyrots;

use \Eyrots\Reducer\ConfigReducer;

class Configuration
{
  protected static $instance = null;
  protected $config;
  protected function __construct($configurationFile, $applyFilters = true)
  {
    $this->config = ConfigReducer::parse($configurationFile, $applyFilters);
  }

  public static function getInstance($configurationFile = null, $applyFilters = true)
  {
    if(null == static::$instance && $applyFilters)
    {
      static::$instance = new static($configurationFile, $applyFilters);
    }

    if(!$applyFilters)
    {
      return new static($configurationFile, $applyFilters);
    }

    return static::$instance;
  }

  public function getConfig()
  {
    return $this->config;
  }
}