<?php
namespace Eyrots;

use \Psr\Container\ContainerInterface;

class Controller
{
  protected $container;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }
  protected function getLogger()
  {
    return $this->container['logger'];
  }
}