<?php
namespace Eyrots\Middleware;

use \Slim\Http\Request;
use \Slim\Http\Response;
use \Eyrots\Configuration;

class CorsMiddleware
{
  public function __invoke(Request $request, Response $response, $next)
  {
    $response = $next($request, $response);
    $config = Configuration::getInstance()->getConfig();
    $corsDomain = $container['settings']['cors.domain'] ?? '*';
    return $response
      ->withHeader('Access-Control-Allow-Origin', $corsDomain)
      ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
      ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  }
}