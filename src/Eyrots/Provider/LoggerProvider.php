<?php
namespace Eyrots\Provider;

use \Pimple\ServiceProviderInterface;
use \Pimple\Container;
use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;

class LoggerProvider implements ServiceProviderInterface
{
  public function register(Container $container)
  {
    $this->instantiateLogger($container);
  }
  protected function instantiateLogger(Container $container)
  {
    $container['logger'] = new Logger('application');
    $container['logger']->pushHandler(new StreamHandler(($container['settings']['appbase'] ?? __DIR__ ) . '/logs/application.log', defined('APP_LOGLEVEL') ? APP_LOGLEVEL : Logger::DEBUG));
  }
}