<?php
namespace Eyrots\Provider;

use \Pimple\ServiceProviderInterface;
use \Pimple\Container;
use \Eyrots\Exception\UndefinedConstantException;

class BaseAppConfigProvider implements ServiceProviderInterface
{
  protected $container;
  public function register(Container $container)
  {
    $this->container = $container;
    $this->defineAppbase();
  }
  protected function defineAppbase()
  {
    if(\defined('EYROTS_BASE'))
    {
      $this->container['settings']['appbase'] = EYROTS_BASE;
    }
    else
    {
      throw new UndefinedConstantException('EYROTS_BASE');
    }
  }
}