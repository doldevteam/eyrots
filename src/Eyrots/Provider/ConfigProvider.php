<?php
namespace Eyrots\Provider;

use \Pimple\ServiceProviderInterface;
use \Pimple\Container;
use \Monolog\Logger;
use \Eyrots\Configuration;

class ConfigProvider implements ServiceProviderInterface
{
  protected $container;
  public function register(Container $container)
  {
    $this->container = $container;
    $this->parseConfig(false);
  }
  public function parseConfig($applyFilters = true)
  {
    if(isset($this->container['configFile']) && ($configFile = $this->container['configFile']))
    {
      $config = Configuration::getInstance($this->container['configFile'], $applyFilters)->getConfig();
      $this->container['settings']->replace($config);
      if(isset($this->container['logger']) && $this->container['logger'] instanceof Logger)
      {
        $this->container['logger']->debug(sprintf("Parsed config from %s:\n%s", $this->container['configFile'], var_export($this->container['settings'], true)));
      }
    }
  }
}