<?php
namespace Eyrots\Provider;

use \Pimple\ServiceProviderInterface;
use \Pimple\Container;
use \Doctrine\Common\Annotations\AnnotationRegistry;
use \Doctrine\Common\Annotations\AnnotationReader;
use \ReflectionClass;
use \Eyrots\Annotation\Route;
use \Eyrots\Reducer\RoutesReducer;
use \Eyrots\App;

class RoutesProvider implements ServiceProviderInterface
{
  protected $container;

  public function register(Container $container)
  {
    $this->container = $container;

    $this->parseRoutes();
  }
  protected function parseRoutes()
  {
    $parsedRoutes = [];
    if(($routes = $this->container['routesFile'] ?? null) && file_exists($routes))
    {
      $routes = RoutesReducer::parse($routes);
      if(is_array($routes) && count($routes) > 0)
      {
        foreach($routes as $path => $route)
        {
          if(!isset($route['action']))
          {
            throw new \Exception('No action defined');
          }
          $parsedRoutes[] = new Route([
            'url' => $path,
            'methods' => $route['methods'] ?? ['GET'],
            'action' => $route['action']
          ]);
        }
      }
    }
    else
    {
      $parsedRoutes = $this->parseRoutesFromAnnotations();
    }
    $this->container['parsedRoutes'] = $parsedRoutes;
  }
  protected function parseRoutesFromAnnotations()
  {
    if(!($skipCache = $this->container['settings']['skipRouteCache'] ?? false) && \file_exists(($routeFile = $this->container['settings']['dirs.cache'] . '/routes.json')))
    {
      return \json_decode(\file_get_contents($routeFile));
    }
    $parsedRoutes = [];
    if(($controllersDir = $this->container['settings']['dirs.controllers'] ?? null))
    {
      AnnotationRegistry::registerLoader('class_exists');
      $reader = new AnnotationReader;
      foreach(
        glob(sprintf('{%1$s/*.php,%1$s/**/*.php}', $controllersDir), GLOB_BRACE)
        as $file
      )
      {
        include($file);
      }

      foreach(get_declared_classes() as $class)
      {
        if(is_subclass_of($class, 'Eyrots\Controller'))
        {
          $reflectionClass = new ReflectionClass($class);
          foreach($reflectionClass->getMethods() as $method)
          {
            $route = $reader->getMethodAnnotation($method, 'Eyrots\Annotation\Route');
            if($route instanceof Route)
            {
              $route->setAction($method);
              $parsedRoutes[] = $route;
            }
          }
        }
      }
    }
    if(!$skipCache)
    {
      \file_put_contents($routeFile, json_encode($parsedRoutes));
    }
    return $parsedRoutes;
  }
  public function mapRoutes(App $app)
  {
    foreach($this->container['parsedRoutes'] as $route)
    {
      $app->map($route->methods, $route->url, $route->action);
    }
  }
}